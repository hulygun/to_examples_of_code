export const api = {
  get (endpoint, data) {},
  post (endpoint, data) {},
  put (endpoint, data) {},
  delete (endpoint, data) {}
}

export const auth = {
  async check (obj) {
    let accessToken = obj.$store.state.auth.accessToken
    if (accessToken) {
      obj.$axios.setHeader('x-access-token', accessToken)
      let check = await obj.$axios.get('/auth/check')
      if (check.data.code === 200) {
        obj.$store.commit('auth/setUser', check.data.user)
      } else {
        this.refresh(obj)
      }
    }
  },
  async refresh (obj) {
    let refreshToken = obj.$store.state.auth.refreshToken
    if (refreshToken) {
      obj.$axios.setHeader('x-refresh-token', refreshToken)
      let refresh = await obj.$axios.post('/auth/refresh')
      if (refresh.data.code === 200) {
        obj.$store.commit('auth/setAccessToken', refresh.data.access_token)
        localStorage.setItem('access_token', refresh.data.access_token)
        this.check(obj)
      }
    }
  },
  async register (obj) {
    let data = await obj.$axios.post('/auth/register', obj.authForm.data)
    if (data.data.code === 200) {
      obj.$store.commit('SetMessage', {color: 'success', text: data.data.message})
      obj.authForm.loginPopUp = false
    } else {
      obj.authForm.error = data.data.errors
      for (let child in obj.$refs.form.inputs) {
        obj.$refs.form.inputs[child].blur()
      }
    }
    obj.authForm.loading = false
  },
  async auth (obj) {
    let data = await obj.$axios.post('/auth/login', obj.authForm.data)
    if (data.data.code === 404) {
      alert(data.data.message)
    } else if (data.data.code === 403) {
      alert(data.data.message)
    } else if (data.data.code === 200) {
      obj.$store.commit('auth/setAccessToken', data.data.access_token)
      localStorage.setItem('access_token', data.data.access_token)
      obj.$store.commit('auth/setRefreshToken', data.data.refresh_token)
      localStorage.setItem('refresh_token', data.data.refresh_token)
      this.check(obj)
      obj.loginPopUp = false
    }
    obj.loading = false
  },
  async restore (obj) {
    let type = (obj.secret) ? 'change_password' : 'restore'
    let data = { email: obj.email }
    if (type === 'change_password') {
      data = {secret: obj.secret, password: obj.password, confirm_password: obj.confirm_password}
    }
    let response = await obj.$axios.post('/auth/' + type, data)
    if (response.data.code === 200) {
      obj.$store.commit('SetMessage', {color: 'success', text: response.data.message})
      obj.$refs.restoreForm.reset()
    } else {
      obj.errors = response.data.errors
      for (let child in obj.$refs.restoreForm.inputs) {
        obj.$refs.restoreForm.inputs[child].blur()
      }
    }
    obj.loading = false
  }

}
