export const state = () => ({
  currentUser: null,
  accessToken: null,
  refreshToken: null
})

export const mutations = {
  setUser (state, userData) {
    state.currentUser = userData
  },
  setAccessToken (state, token) {
    if (typeof (token) !== 'undefined') {
      state.accessToken = token
    }
  },
  setRefreshToken (state, token) {
    state.refreshToken = token
  }
}
