export const state = () => ({
  user: null,
  message: {
    status: false,
    color: '',
    mode: '',
    text: ''
  }
})

export const mutations = {
  SET_USER (state, userData) {
    state.user = userData
  },
  SetMessage (state, messageObj) {
    state.message = {status: true, color: messageObj.color, mode: '', text: messageObj.text}
  },
  clearMessage (state) {
    state.message = {
      status: false,
      color: '',
      mode: '',
      text: ''
    }
  }
}
