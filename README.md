* Node v8.6.0
* Python >= v3.5

# Локальная развёртка #

## Бек ##
1. `git@gitlab.com:expertizzo/expertizzo.git ./` Клонируем репозиторий
1. `pip install -r requirements.txt` Установка зависимостей python
1. `./manage.py migrate` Запуск миграций
1. `./manage.py runserver` Запуск dev сервера

## Фронт ##
1. `cd front` Переходим во фронт
1. `npm i` Установка необходимых nodejs модулей для фронта
1. `npm run dev` Запуск дев сервера

> В **front/nuxt.config** прописан **browserBaseURL** для обращений к python серверу, как *http://localhost:8000*


# Используемые инструменты #

* [Django](https://www.djangoproject.com/) - python framework для API сервера
* [Nuxtjs](https://ru.nuxtjs.org/) - javascript framework для фронта с SSR, основанный на *Vuejs*
* [vuefity](https://vuetifyjs.com/) - Набор компонентов в стиле Material Design для *Vuejs*
* [Pug](https://pugjs.org/api/getting-started.html) - Шаблонизатор