#!/usr/bin/env python
import os
import sphinx_bootstrap_theme
import sys


def rel(*x):
    return os.path.join(os.path.abspath(os.path.dirname(__file__)), *x)

sys.path.insert(0, rel('..'))
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "cfg.settings")

from django import setup as django_setup
django_setup()

extensions = [
    'sphinx.ext.autodoc',
    'sphinx.ext.doctest',
    'sphinx.ext.coverage',
    'sphinx.ext.intersphinx',
    'sphinx.ext.viewcode',
]

autodoc_member_order = 'bysource'
autodoc_default_flags = ['members']
intersphinx_mapping = {
    'python': ('http://python.readthedocs.org/en/latest/', None),
    'django': ('http://docs.djangoproject.com/en/1.11/', 'https://docs.djangoproject.com/en/1.11/_objects/'),
    'sphinx': ('http://sphinx.readthedocs.org/en/latest/', None),
}

templates_path = ['.templates']
source_suffix = '.rst'
master_doc = 'index'

project = 'expertizzo'
copyright = '2017, hulygun'
author = 'hulygun'

version = '1.0'
release = '1.0'
language = 'ru'
exclude_patterns = []

pygments_style = 'sphinx'

todo_include_todos = True

html_theme = 'bootstrap'
html_theme_path = sphinx_bootstrap_theme.get_html_theme_path()
html_static_path = ['.static']
html_sidebars = {
    '**': [
        'relations.html',  # needs 'show_related': True theme option to display
        'searchbox.html',
    ]
}
htmlhelp_basename = 'expertizzodoc'

man_pages = [
    (master_doc, 'expertizzo', 'expertizzo Documentation',
     [author], 1)
]

texinfo_documents = [
    (master_doc, 'expertizzo', 'expertizzo Documentation',
     author, 'expertizzo', 'One line description of project.',
     'Miscellaneous'),
]


