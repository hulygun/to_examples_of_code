from django.conf.urls import url

from .views import AuthEndPoint

urlpatterns = [
    url(r'(?P<method>\w+)', AuthEndPoint.as_view(), name='auth'),
]