import uuid

from datetime import datetime, timedelta

import binascii

from django.conf import settings
from django.contrib.auth.hashers import make_password, check_password
from django.db import models

from alc import ALCMixin
from emails.models import EmailTemplate


class Profile(models.Model, ALCMixin):
    user = models.OneToOneField('User')
    username = models.CharField(max_length=20, unique=True, null=True)
    ava = models.CharField(max_length=255, unique=True, null=True)
    created = models.DateTimeField(auto_now_add=True)

    class Meta:
        db_table = 'profiles'


class UserManager(models.Manager):
    def create(self, email, password):
        user = self.model(email=email)
        user.set_password(password)
        user.save()
        Profile.objects.create(user=user)
        return user

    def restore_password(self, hex_password, new_password):
        password = binascii.unhexlify(hex_password.encode('utf-8')).decode('utf-8')
        try:
            user = self.get(password=password)
            user.set_password(new_password)
            user.save()
            return True
        except self.model.DoesNotExist:
            return False


class User(models.Model):
    """
    **Модель пользователя сервиса**

    :parameter models.EmailField email: Электронная почта
    :parameter models.CharField password: Пароль
    :parameter models.UUIDField access_token: Токен доступа к контенту
    :parameter models.UUIDField refresh_token: Токен для сброса *access_token*
    :parameter models.BooleanField is_active: Флаг подтверждённого пользователя
    """
    email = models.EmailField(unique=True)
    password = models.CharField(max_length=50)

    access_token = models.UUIDField(default=uuid.uuid1)
    refresh_token = models.UUIDField(default=uuid.uuid1)

    is_active = models.BooleanField(default=False)

    objects = UserManager()

    class Meta:
        db_table = 'users'

    def set_password(self, password):
        """
        Устанавливает пароль для пользователя

        :param str password: Новый пароль
        """
        self.password = make_password(password)

    def check_password(self, password):
        """
        Проверяет правильность пароля

        :param str password: пароль
        :rtype: bool
        """
        return check_password(password, self.password)

    def check_token(self, name, seconds=None):
        """
        Проверяет актуальность токена по его имени

        :param str name: Токен  (*access_token*, *refresh_token*)
        :param int seconds: Время жизни токена в секундах
        :rtype: bool
        """
        seconds = seconds or settings.TOKEN_EXPIRATION.get(name)
        token_time = (getattr(self, name).time - 0x01b21dd213814000) * 100 / 1e9  # Время создания токена в timestamp
        experation_time = datetime.fromtimestamp(token_time) + timedelta(seconds=seconds)
        return experation_time > datetime.now()

    def update_token(self, name):
        """
        Обновляет значение токена по его имени

        :param str name: Токен  (*access_token*, *refresh_token*)
        """
        setattr(self, name, uuid.uuid1())

    def get_restore_url(self):
        """
        Возвращает урл для восстановления пароля

        :rtype: str
        """
        token = binascii.hexlify(self.password.encode('utf-8')).decode('utf-8')
        return '{}/auth/restore_password?secret={}'.format(settings.FRONTEND_BASE, token)

    def get_confirm_register_url(self):
        """
        Возвращает урл для подтверждения регистрации

        :rtype: str
        """
        return '{}/auth/confirm_register?secret={}'.format(settings.FRONTEND_BASE, self.access_token.hex)

    def send_mail(self, email_type):
        """
        Отправляет email пользователю

        :param str email_type: Тип письма
        """
        EmailTemplate.objects.send_mail(email_type, self.email, self)

    def __str__(self):
        return self.email
