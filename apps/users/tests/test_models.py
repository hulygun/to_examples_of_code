from time import sleep

from django.core import mail
from django.test import TestCase

from ..models import User, Profile


class UserTestCase(TestCase):
    fixtures = ['emails.EmailTemplate.json']

    def setUp(self):
        self.user = User.objects.create('local@local.ru', '1234')

    def test_password(self):
        password = 'password'
        self.user.set_password(password)
        self.user.save()

        self.assertTrue(self.user.check_password(password))
        self.assertFalse(self.user.check_password('wrong password'))

    def test_token(self):
        self.assertTrue(self.user.check_token('access_token'))
        self.assertTrue(self.user.check_token('refresh_token'))
        sleep(2)
        self.assertFalse(self.user.check_token('access_token', 1))
        self.assertFalse(self.user.check_token('refresh_token', 1))
        self.user.update_token('access_token')
        self.user.update_token('refresh_token')
        self.user.save()
        self.assertTrue(self.user.check_token('access_token', 1))
        self.assertTrue(self.user.check_token('refresh_token', 1))

    def test_restore_password(self):
        password_hex = self.user.get_restore_url().split('=')[-1]
        self.assertTrue(User.objects.restore_password(password_hex, 'new_password'))
        self.assertFalse(User.objects.restore_password(password_hex, 'renew_password'))
        self.assertTrue(User.objects.get(pk=self.user.pk).check_password('new_password'))

    def test_confirm(self):
        access_token_hex = self.user.get_confirm_register_url().split('=')[-1]
        self.assertEqual(self.user.access_token.hex, access_token_hex)

    def test_send_email(self):
        self.assertEqual(len(mail.outbox), 0)
        self.user.send_mail('register')
        self.assertEqual(len(mail.outbox), 1)

    def test_user_repr(self):
        self.assertEqual(self.user.__str__(), 'local@local.ru')

class ProfileTestCase(TestCase):
    def setUp(self):
        User.objects.create('test@local.ru', '1234')

    def test_profile_autocreate(self):
        user = User.objects.get(email='test@local.ru')
        self.assertTrue(Profile.objects.filter(user_id=user.pk).exists())
