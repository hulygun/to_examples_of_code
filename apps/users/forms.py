from django import forms

from emails.const import EmailType
from .models import User


class RegisterForm(forms.ModelForm):
    confirm_password = forms.CharField()

    def clean_email(self):
        if User.objects.filter(email=self.cleaned_data.get('email')).exists():
            self.add_error('email', 'Пользователь с email {} уже существует'.format(self.cleaned_data.get('email')))
        return self.cleaned_data.get('email')

    def clean_confirm_password(self):
        cleaned_data = self.cleaned_data
        if cleaned_data.get('password') != cleaned_data.get('confirm_password'):
            self.add_error('confirm_password', 'Пароли не совпадают')
        return cleaned_data.get('confirm_password')

    def save(self):
        self.cleaned_data.pop('confirm_password')
        user = User.objects.create(**self.cleaned_data)
        user.send_mail(EmailType.REGISTER)


    class Meta:
        model = User
        fields = ['email', 'password']
