import json

from django.conf import settings
from django.core.exceptions import ValidationError
from django.http import JsonResponse
from django.views import View

from emails.const import EmailType
from .const import CAN_REFRESH
from .forms import RegisterForm
from .models import User


class AuthEndPoint(View):
    def dispatch(self, request, *args, **kwargs):
        try:
            data = getattr(self, kwargs.get('method'))(request, *args, **kwargs)
        except AttributeError:
            data = {'code': 418, 'message': 'Неверный endpoint'}
        return JsonResponse(data)

    def register(self, request, *args, **kwargs):
        """
        Регистрация пользователя
        """
        if request.method == 'POST':
            form = RegisterForm(json.loads(request.body))
            if not form.is_valid():
                return {'code': 412, 'errors': {key: '\n'.join(form.errors.get(key, '')) for key in ['email', 'password', 'confirm_password']}}
            else:
                form.save()
                return {'code': 200, 'message': 'Пользователь успешно создан. Проверьте электронную почту'}
        else:
            return {'code': 405, 'message': 'Метод не поддерживается'}

    def confirm(self, request, *args, **kwargs):
        """
        Подтверждение регистрации
        """
        if request.method == 'POST':
            try:
                user = User.objects.get(access_token=json.loads(request.body).get('secret'))
                if user.check_token('access_token', settings.TOKEN_EXPIRATION.get('confirm')):
                    user.is_active = True
                    user.update_token('access_token')
                    user.save()
                    return {'code': 200, 'access_token': user.access_token.hex, 'refresh_token': user.refresh_token.hex}
                else:
                    return {'code': 406, 'message': 'Время действия подтверждающей ссылки истекло'}
            except (User.DoesNotExist, ValidationError):
                return {'code': 412, 'message': 'Неверный код подтверждения'}
        else:
            return {'code': 405, 'message': 'Метод не поддерживается'}

    def restore(self, request, *args, **kwargs):
        """
        Восстановление пароля
        """
        if request.method == 'POST':
            try:
                user = User.objects.get(email=json.loads(request.body).get('email'))
                user.send_mail(EmailType.RESTORE_PASSWORD)

                return {'code': 200, 'message': 'Проверьте почту'}
            except User.DoesNotExist:
                return {'code': 412, 'errors': {'email': 'Пользователь не найден'}}
        else:
            return {'code': 405, 'message': 'Метод не поддерживается'}

    def change_password(self, request, *args, **kwargs):
        """
        Смена пароля
        """
        if request.method == 'POST':
            data = json.loads(request.body)
            if data.get('password') != data.get('confirm_password'):
                return {'code': 412, 'errors': {'confirm_password': 'Пароли не совпадают'}}
            if User.objects.restore_password(data.get('secret'), data.get('password')):
                return {'code': 200, 'message': 'Смена пароля прошла успешно'}
            else:
                return {'code': 401, 'message': 'Ссылка недействительна'}
        else:
            return {'code': 405, 'message': 'Метод не поддерживается'}

    def login(self, request, *args, **kwargs):
        """
        Авторизация по паролю
        """
        if request.method == 'POST':
            data = json.loads(request.body)
            try:
                user = User.objects.get(email=data.get('email'))
                if user.check_password(data.get('password')):
                    need_save = False
                    for token in ('access_token', 'refresh_token',):
                        if not user.check_token(token):
                            user.update_token(token)
                            need_save = True
                    if need_save:
                        user.save()
                    return {'code': 200, 'access_token': user.access_token.hex, 'refresh_token': user.refresh_token.hex}
                else:
                    return {'code': 403, 'message': 'Пароль неверный'}
            except:
                return {'code': 404, 'message': 'Пользователь не найден'}
        else:
            return {'code': 405, 'message': 'Метод не поддерживается'}

    def check(self, request, *args, **kwargs):
        """
        Проверка пользователя
        """
        data = {'code': request.response_code}
        if request.user:
            if request.user.is_active:
                # ToDo: Впилить поля из профиля
                data['user'] = {'email': request.user.email}
            else:
                data['code'] = 401
                data['message'] = 'Пользователь неактивен'
        else:
            # ToDo: Впилить тексты сообщений на основании response_code
            data['message'] = 'Сообщение об ошибке на основании response_code'

        return data

    def refresh(self, request, *args, **kwargs):
        """
        Сброс access_token
        """
        if request.user and request.user.is_active and request.response_code == CAN_REFRESH:
            request.user.update_token('access_token')
            request.user.save()
            return {'code': 200, 'access_token': request.user.access_token.hex}
        else:
            # ToDo: Впилить тексты сообщений на основании response_code
            return {'code': request.response_code, 'message': 'Сообщение об ошибке на основании response_code'}
