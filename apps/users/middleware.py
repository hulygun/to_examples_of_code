import json

from django.core.exceptions import ValidationError

from .const import (USER_NOT_FOUND, ACCESS_TOKEN_INVALID, REFRESH_TOKEN_INVALID, USER_AUTHORIZED, ACCESS_TOKEN_EXPIRED,
                    CAN_REFRESH, REFRESH_TOKEN_EXPIRED
                    )
from .models import User


class AccessTokenMiddleware(object):
    def __init__(self, get_response):
        self.get_response = get_response

    def __call__(self, request):
        request.user = user = None
        request.response_code = response_status = USER_NOT_FOUND

        access_token = request.META.get('HTTP_X_ACCESS_TOKEN') or request.GET.get('access_token')
        refresh_token = request.META.get('HTTP_X_REFRESH_TOKEN') or request.GET.get('refresh_token')

        if request.body:
            data = json.loads(request.body)
            access_token = access_token or data.get('access_token')
            refresh_token = refresh_token or data.get('refresh_token')

        try:
            user = User.objects.get(access_token=access_token)
        except (User.DoesNotExist, ValidationError):
            response_status = ACCESS_TOKEN_INVALID

        if refresh_token:
            try:
                user = User.objects.get(refresh_token=refresh_token)
            except (User.DoesNotExist, ValidationError):
                response_status = REFRESH_TOKEN_INVALID

        if user and access_token:
            response_status = USER_AUTHORIZED if user.check_token('access_token') else ACCESS_TOKEN_EXPIRED
        elif user and refresh_token:
            response_status = CAN_REFRESH if user.check_token('refresh_token') else REFRESH_TOKEN_EXPIRED



        request.user = user
        request.response_code = response_status
        response = self.get_response(request)

        return response
