from functools import reduce


class UserRole:
    """Пользовательские роли"""
    ANONIM = 0
    AUTHORIZED = 1
    AUTHOR = 2


class ContentAction:
    """Уровни доступа к контенту"""
    VIEW_PUBLISH = 0
    VIEW_ALL = 1
    CREATED = 2
    EDIT = 3
    PUBLISH = 4


class ALCMixin(object):
    """Примесь для работы с ALC"""
    PERM_VIEW_PUBLISH = b'111'
    PERM_VIEW_ALL = b'001'
    PERM_CREATED = b'011'
    PERM_EDIT = b'001'
    PERM_PUBLISH = b'000'

    USER_FIELD = 'user'

    def __init__(self):
        super(ALCMixin, self).__init__()
        self._validate_perms()

    @property
    def _permissions(self):
        """
        Строка с правами доступа на основе

        - ALCMixin.PERM_VIEW_PUBLISH
        - ALCMixin.PERM_VIEW_ALL
        - ALCMixin.PERM_CREATED
        - ALCMixin.PERM_EDIT
        - ALCMixin.PERM_PUBLISH

        :rtype: bin
        """
        raw_perms = (self.PERM_VIEW_PUBLISH, self.PERM_VIEW_ALL, self.PERM_CREATED, self.PERM_EDIT, self.PERM_PUBLISH)
        return reduce(lambda x, y: x + y, raw_perms, b'')

    def _validate_perms(self):
        """
        Валидация размера строки с правами доступа

        :except alc.ALCExeption: Если не совпадает размер ALCMixin._permissions с ожидаемым
        """
        if type_length(UserRole) * type_length(ContentAction) != len(self._permissions):
            raise ALCExeption(type_length(UserRole) * type_length(ContentAction), len(self._permissions))

    def can(self, action, user=None):
        """
        Проверка доступа к контенту по роли и типу доступа

        :param int action: Тип доступа (ContentAction)
        :param users.User user: Пользователь
        :rtype: bool
        """
        who = UserRole.ANONIM
        if user:
            who = UserRole.AUTHOR if user == getattr(self, self.USER_FIELD, None) else UserRole.AUTHORIZED

        split_roles = len([attr for attr in UserRole.__dict__ if not attr.startswith('_')])
        return bool(self._permissions[who::split_roles][action])


class ALCExeption(Exception):
    def __init__(self, expected, got):
        self.expected = expected
        self.got = got
        super(ALCExeption, self).__init__('Wrong permision len. Expected: {}, got {}'.format(expected, got))


def type_length(object_type):
    return len([attr for attr in object_type.__dict__ if not attr.startswith('_')])