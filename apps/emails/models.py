from django.core.mail import send_mail
from django.db import models
from django.template import Template, Context


class EmailManager(models.Manager):
    def send_mail(self, name, to, obj, extra_contex=None):
        template = self.get(name=name)
        title, body = template.build_email(obj, extra_contex)
        send_mail(title, body, template.author, list(to), html_message=body)


class EmailTemplate(models.Model):
    name = models.CharField(max_length=20, unique=True)
    title = models.CharField(max_length=100)
    author = models.EmailField(max_length=50)
    body = models.TextField()

    objects = EmailManager()

    class Meta:
        db_table = 'email_templates'

    def __str__(self):
        return self.name

    def build_email(self, obj, extra_context):
        body = Template(self.body)
        title = Template(self.title)
        raw_context = {obj._meta.model_name: obj}
        if extra_context:
            raw_context.update(extra_context)
        context = Context(raw_context)
        return title.render(context), body.render(context)
