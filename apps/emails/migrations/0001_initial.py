# -*- coding: utf-8 -*-
# Generated by Django 1.11.6 on 2017-10-22 22:54
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='EmailTemplate',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=20, unique=True)),
                ('title', models.CharField(max_length=100)),
                ('author', models.EmailField(max_length=50)),
                ('body', models.TextField()),
            ],
            options={
                'db_table': 'email_templates',
            },
        ),
    ]
