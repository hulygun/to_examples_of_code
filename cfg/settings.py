import environ
from corsheaders.defaults import default_headers

root = environ.Path(__file__) - 2
env = environ.Env(DEBUG=(bool, False), )
environ.Env.read_env(root('.env'))

SITE_ROOT = root()

DEBUG = env('DEBUG')

DATABASES = {
    'default': env.db(),
    'test': env.db('TEST_DATABASE_URL')
}

SECRET_KEY = env('SECRET_KEY')

INSTALLED_APPS = [
    # contrib
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',

    # third party
    'django_extensions',
    'corsheaders',

    # project
    'users',
    'emails'

]

MIDDLEWARE = [
    'django.middleware.security.SecurityMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'corsheaders.middleware.CorsMiddleware',
    'django.middleware.common.CommonMiddleware',
    # 'django.middleware.csrf.CsrfViewMiddleware',
    # 'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
    'users.middleware.AccessTokenMiddleware'
]

ROOT_URLCONF = 'cfg.urls'

TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [root('apps/core/templates')],
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                'django.template.context_processors.debug',
                'django.template.context_processors.request',
                'django.contrib.auth.context_processors.auth',
                'django.contrib.messages.context_processors.messages',
            ],
        },
    },
]

WSGI_APPLICATION = 'cfg.wsgi.application'

AUTH_PASSWORD_VALIDATORS = []

LANGUAGE_CODE = 'ru-ru'

TIME_ZONE = 'Europe/Moscow'

USE_I18N = True

USE_L10N = True

USE_TZ = True

STATIC_URL = '/static/'
FIXTURE_DIRS = [root('fixtures')]

CORS_ORIGIN_ALLOW_ALL = DEBUG

CORS_ORIGIN_WHITELIST = (
    'localhost:8000',
    '127.0.0.1:3000'
)

FRONTEND_BASE = env('FRONTEND_BASE')

if DEBUG:
    EMAIL_BACKEND = 'django.core.mail.backends.console.EmailBackend'

CORS_ALLOW_HEADERS = default_headers + ('x-access-token', 'x-refresh-token')

TOKEN_EXPIRATION = {
    'access_token': 60 * 60 * 24,
    'refresh_token': 60 * 60 * 24 * 7,
    'confirm': 60 * 60 * 24 * 3
}
